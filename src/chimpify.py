#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import tkinter, tkinter.simpledialog
import imaplib
import email
import getpass
from collections import Counter
import sys


STUDREG_CAMPUS_TO_MAILCHIMP_LIST = {
    "1": "0bc4b12006",  # med
    "2": "4ce4f14393",  # rehovot
    "3": "00c7318189",  # har
    "4": "1263fbdfab",  # givat ram
}


HEADERS_ORIGINAL = [
    '''שם משפחה''',
    '''שם פרטי''',
    '''זהות''',
    '''דוא-ל''',
    '''טלפון''',
    '''טלפון נייד''',
    '''כתובת''',
    '''מיקוד''',
    '''קמפוס''',
    '''סמל פקולטה''',
    '''פקולטה''',
    '''סמל תואר''',
    '''תואר''',
    '''שנת לימוד''',
    '''סמל חוג 1''',
    '''חוג 1''',
    '''סמל חוג 2''',
    '''חוג 2''',
    '''שנת לידה''',
    '''מין''',
    '''משפחה באנגלית''',
    '''פרטי באנגלית''',
    '''אגודת הסטודנטים''',
]

FROM_STUDREG_TO_ORIGINAL_COLUMN_NUMBER = [
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    11,
    13,
    14,
    16,
    18,
    19,
    20,
    21,
    22,
]

FROM_CHIMP_TO_ORIGINAL_COLUMN_NUMBER = [0, 1, 3, 8, 9, 11, 13, 14, 16, 22]

CHIMP_HEADERS = [
    "Lname",
    "Fname",
    "email",
    "campus",
    "faculty",
    "degree",
    "year",
    "main",
    "secondary",
    "revaha",
]

STUDREG_HEADERS = [
    "id",
    "fname",
    "lname",
    "email",
    "tel",
    "campus",
    "faculty",
    "degree",
    "year",
    "major",
    "minor",
    "revaha",
    "miluim",
]


class ORIGINAL_CSV:
    CAMPUS = 8
    FACULTY = 10
    AGUDA_MEMBERSHIP = 22


class STUDREG_CSV:
    CAMPUS = 8
    ID = 2


def load_file(filename):
    with open(filename, encoding='windows-1255') as f:
        data = [n.split('\t') for n in f.readlines()[1:]]
        data = [[rubrik.strip() for rubrik in n] for n in data]
        data = [
            [
                ''.join(v.split('-'))
                if i == STUDREG_CSV.ID
                else v
                for i, v in enumerate(row)
            ] for row in data
        ]
    return data


KHUG_NAME_TO_NUMBER = {
    'ג.רם': "3",
    'עין כרם': "1",
    'רחובות': "2",
    'הר הצופים': "4",
}


def convert_original_to_studreg(data):
    new_data = []
    for line in data:
        new_line = []
        for i in FROM_STUDREG_TO_ORIGINAL_COLUMN_NUMBER:
            if i == ORIGINAL_CSV.CAMPUS:
                new_line.append(KHUG_NAME_TO_NUMBER.get(line[i], line[i]))
            else:
                new_line.append(line[i])
        new_data.append(new_line)
    return new_data


def convert_original_to_chimp(data, campus):
    new_data = []
    for line in data:
        line_campus_number = KHUG_NAME_TO_NUMBER.get(
                line[ORIGINAL_CSV.CAMPUS],
                line[ORIGINAL_CSV.CAMPUS])
        if line_campus_number == str(campus):
            new_line = []
            for i in FROM_CHIMP_TO_ORIGINAL_COLUMN_NUMBER:
                if i == ORIGINAL_CSV.CAMPUS:
                    new_line.append(line_campus_number)
                else:
                    new_line.append(line[i])
            new_data.append(new_line)
    return new_data


def print_membership(data, by):
    count_membership = Counter()
    for line in data:
        count_membership = count_membership + Counter({line[
            by]: int(line[ORIGINAL_CSV.AGUDA_MEMBERSHIP])})
    for name, count in sorted(count_membership.items()):
        print(name + ':', count)


def print_aguda_registration_stats(filename):
    data = load_file(filename)

    print_membership(data, by=ORIGINAL_CSV.FACULTY)
    print()
    print_membership(data, by=ORIGINAL_CSV.CAMPUS)
    print()
    print('סה"כ:', sum(int(n[ORIGINAL_CSV.AGUDA_MEMBERSHIP]) for n in data))


def studregify(filename):
    "Convert original CSV (actually TSV) to studreg's format"
    data = load_file(filename)
    with open("{}-studreg.csv".format(filename), 'w') as f:
        new_data = convert_original_to_studreg(data)
        for line in new_data:
            print(*line, sep='\t', file=f)


def chimpify(filename):
    "Convert original CSV to chimp"
    data = load_file(filename)
    for campus in range(1,5):
        with open("{}-campus-{}.csv".format(filename, campus), 'w') as f:
            print(*CHIMP_HEADERS, sep='\t', file=f)
            new_data = convert_original_to_chimp(data, campus)
            for line in new_data:
                print(*line, sep='\t', file=f)


def make_files(filename):
    studregify(filename)
    chimpify(filename)


def get_latest_file(login):
    m = imaplib.IMAP4_SSL('imap.gmail.com')
    m.login(*login)
    m.select('CSV')
    resp, data = m.search(None, '(ALL)')
    emailID = data[0].split()[-1]
    resp, data = m.fetch(emailID, "(BODY.PEEK[])")
    email_body = data[0][1]
    mail = email.message_from_bytes(email_body)
    if mail.get_content_maintype() != "multipart":
        return
    for part in mail.walk():
        if (part.get("Content-Disposition") is not None and
                part.get("Content-Disposition").startswith("attachment")):
            filename = part.get_filename()
            print(filename)
            with open(part.get_filename(), 'wb') as f:
                f.write(part.get_payload(decode=True))
    return filename


if __name__ == "__main__":
    root = tkinter.Tk()
    root.withdraw()
    login = [
        "webmaster@hafakulta.org",
        tkinter.simpledialog.askstring("webmaster@hafakulta.org password",
            "webmaster@hafakulta.org password:", show="*")
    ]
    filename = get_latest_file(login)
    make_files(filename)
