{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Codec.Text.IConv
import qualified Data.ByteString            as Strict
import qualified Data.ByteString.Lazy       as Lazy
import           Data.ByteString.Lazy.Char8 (unpack)
import           Data.Char                  (ord)
import           Data.Csv                   (FromRecord, HasHeader (NoHeader),
                                             ToRecord, decDelimiter, decodeWith,
                                             defaultDecodeOptions)
import           Data.Function
import           Data.List                  (drop, isPrefixOf, maximumBy)
import           Data.Text                  (Text)
import           Data.Vector                (Vector)
import           GHC.Generics
import           Options.Applicative
import           System.Directory           (getDirectoryContents)
import           System.Environment

data Student =
	Student
		{ studentA :: !Text
		, studentB :: !Text
		, studentC :: !Text
		, studentD :: !Text
		, studentE :: !Text
		, studentF :: !Text
		, studentG :: !Text
		, studentH :: !Text
		, studentI :: !Text
		, studentJ :: !Text
		, studentL :: !Text
		, studentM :: !Text
		, studentN :: !Text
		, studentO :: !Text
		, studentP :: !Text
		, studentQ :: !Text
		, studentR :: !Text
		, studentS :: !Text
		, studentT :: !Text
		-- , studentU :: !Text
		-- , studentV :: !Text
		-- , studentW :: !Text
		-- , studentX :: !Text
		-- , studentY :: !Text
		-- , studentZ :: !Text
		} deriving (Show, Generic)

instance FromRecord Student
instance ToRecord Student

convertDate ::
	String
	-> String
convertDate date =
  year ++ month ++ day
  where
	day = Prelude.take 2 date
	month = Prelude.take 2 $ Prelude.drop 2 date
	year = Prelude.take 4 $ Prelude.drop 4 date

getLatestFile ::
	FilePath
	-> IO FilePath
getLatestFile dataDir = do
	directoryContents <- getDirectoryContents dataDir
	let dataFiles =  filter (isPrefixOf "A_STUDENTIM_") directoryContents :: [String]
	let dataDates = fmap (convertDate . Prelude.drop 12) dataFiles
	return . fst . maximumBy (compare `on` snd) $ Prelude.zip dataFiles dataDates

readTSV ::
	Lazy.ByteString
	-> Either String (Vector Student)
readTSV = decodeWith options NoHeader
	where
	options = defaultDecodeOptions
		{ decDelimiter = fromIntegral $ ord '\t' }


loadRawData ::
	FilePath
	-> IO Lazy.ByteString -- ByteString
loadRawData dataDir = do
	latestFile <- getLatestFile dataDir
	text <- readCSVFile latestFile
	let (Left convertedText) = convertStrictly "windows-1255" "UTF-8" text :: Either Lazy.ByteString ConversionError
	return convertedText
	where
	readCSVFile = Lazy.readFile . ("data/" ++)

data CLIArgs =
	CLIArgs
		{ dataPath :: FilePath
		}

cliArgs ::
	Parser CLIArgs
cliArgs =
	CLIArgs
		<$> strOption
			( long "data"
			<> metavar "DATA"
			<> help "Path to data file"
			)

main :: IO ()
main = execParser cliArgs >>= f

f ::
	cliArgs
	-> IO ()
f args = do
	let rawData = loadRawData . dataPath $ args
	let char8Data = unpack rawData
	let (Right tsvData) = readTSV rawData
	-- Strict.putStrLn sourceData
	-- Lazy.writeFile ("output/" ++ latestFile ++ ".utf8.csv") convertedText
	print tsvData
